let http = require('http');

let port = 3000;

http.createServer( function (request, response) {
  if (request.url === '/home') {
    response.write('welcome to my world!');
    response.end();
  } else if (request.url === '/login') {
    response.write(`Bienvenido a mi sitio web!`); 
    response.end();
  } else if (request.url === '/register') {
    response.write('Here you can register');
    response.end();
  } else if (request.url === '/profile') {
    response.write('Create your profile');
    response.end();
  } else {
    response.write('Run! Run! Run!');
    response.end();
  }
}).listen(port); 




console.log(`server is running successfully: ${port}`);

